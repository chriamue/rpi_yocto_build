FROM ubuntu:16.04

WORKDIR /rpi

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get autoremove -y --purge && \
    apt-get install -y cpio python3 gawk wget git-core diffstat unzip texinfo gcc-multilib \
        build-essential chrpath python vim locales
RUN locale-gen en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV YOCTO_VERSION dunfell

RUN git clone -b $YOCTO_VERSION git://git.yoctoproject.org/poky.git poky && \
    git clone -b $YOCTO_VERSION git://git.openembedded.org/meta-openembedded --depth=1 && \
    git clone -b $YOCTO_VERSION git://git.yoctoproject.org/meta-raspberrypi --depth=1
RUN mkdir -p rpi64/conf && touch rpi64/conf/sanity.conf && /bin/bash -c "source poky/oe-init-build-env rpi64 && \
    bitbake-layers add-layer ../meta-raspberrypi && \
    bitbake-layers add-layer ../meta-openembedded/meta-oe && \
    bitbake-layers add-layer ../meta-openembedded/meta-filesystems"

ENV MACHINE raspberrypi3-64

RUN /bin/bash -c "source poky/oe-init-build-env rpi64 && \
    bitbake core-image-minimal"